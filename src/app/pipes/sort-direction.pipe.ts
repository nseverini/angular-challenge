import { Pipe, PipeTransform } from '@angular/core';
import { ISortDirection } from '../models/sort-direction';

@Pipe({
  name: 'sortDirection',
})
export class SortDirectionPipe implements PipeTransform {
  transform(
    columnName: string,
    sortColumn: string,
    sortDirection: ISortDirection
  ): string {
    if (columnName !== sortColumn) return '';

    return sortDirection === ISortDirection.Asc ? '⬆️' : '⬇️';
  }
}
