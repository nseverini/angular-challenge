import { SortDirectionPipe } from './sort-direction.pipe';
import { ISortDirection } from '../models/sort-direction';

describe('SortDirectionPipe', () => {
  let pipe: SortDirectionPipe;

  beforeEach(() => {
    pipe = new SortDirectionPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return empty string if columnName is different from sortColumn', () => {
    const columnName = 'email';
    const sortColumn = 'username';
    const sortDirection: ISortDirection = ISortDirection.Asc;

    const result = pipe.transform(columnName, sortColumn, sortDirection);

    expect(result).toEqual('');
  });

  it('should return ⬆️ if sortDirection is Asc', () => {
    const columnName = 'surname';
    const sortColumn = 'surname';
    const sortDirection: ISortDirection = ISortDirection.Asc;

    const result = pipe.transform(columnName, sortColumn, sortDirection);

    expect(result).toEqual('⬆️');
  });

  it('should return ⬇️ if sortDirection is Desc', () => {
    const columnName = 'firstname';
    const sortColumn = 'firstname';
    const sortDirection: ISortDirection = ISortDirection.Desc;

    const result = pipe.transform(columnName, sortColumn, sortDirection);

    expect(result).toEqual('⬇️');
  });
});
