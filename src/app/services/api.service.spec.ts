import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import { ApiService } from './api.service';
import { IUser, IUserExtended } from '../models/user';

describe('ApiService', () => {
  let service: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService],
    });
    service = TestBed.inject(ApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getUsers', () => {
    it('should return an Observable of IUserExtended array', () => {
      const mockUsers: IUser[] = [
        {
          id: 1,
          name: 'Nahuel Severini',
          username: 'nseverini',
          email: 'nseverini@hotmail.com.ar',
          phone: '',
          website: 'https://gitlab.com/nseverini/angular-challenge',
          company: {
            name: '',
            catchPhrase: '',
            bs: '',
          },
          address: {
            street: '',
            suite: '',
            city: '',
            zipcode: '',
            geo: {
              lat: '',
              long: '',
            },
          },
        },
      ];
      const transformedUsers: IUserExtended[] = [
        {
          id: 1,
          name: 'Nahuel Severini',
          firstname: 'Nahuel',
          surname: 'Severini',
          username: 'nseverini',
          email: 'nseverini@hotmail.com.ar',
          phone: '',
          website: 'https://gitlab.com/nseverini/angular-challenge',
          company: {
            name: '',
            catchPhrase: '',
            bs: '',
          },
          address: {
            street: '',
            suite: '',
            city: '',
            zipcode: '',
            geo: {
              lat: '',
              long: '',
            },
          },
        },
      ];

      service.getUsers().subscribe((users) => {
        expect(users).toEqual(transformedUsers);
      });

      const request = httpMock.expectOne(environment.apiUrl);
      expect(request.request.method).toBe('GET');
      request.flush(mockUsers);
    });

    it('should handle empty response', () => {
      const mockUsers: IUser[] = [];

      service.getUsers().subscribe((users) => {
        expect(users).toEqual([]);
      });

      const request = httpMock.expectOne(environment.apiUrl);
      expect(request.request.method).toBe('GET');
      request.flush(mockUsers);
    });

    it('should handle HTTP error', () => {
      const errorMessage = 'Error fetching users';

      service.getUsers().subscribe({
        error: (error) => {
          expect(error.statusText).toBe(errorMessage);
        },
      });

      const request = httpMock.expectOne(environment.apiUrl);
      expect(request.request.method).toBe('GET');
      request.flush(null, { status: 500, statusText: errorMessage });
    });
  });
});
