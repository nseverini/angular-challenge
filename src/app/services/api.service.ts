import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IUser, IUserExtended } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private http = inject(HttpClient);

  getUsers(): Observable<IUserExtended[]> {
    return this.http.get<IUser[]>(environment.apiUrl).pipe(
      map((users) => {
        return users.map((user) => this.transformUser(user));
      })
    );
  }

  transformUser(user: IUser): IUserExtended {
    const { id, phone, website, company, address, username, email, name } =
      user;
    const [firstname, surname] = name.split(' ');

    return {
      id,
      username,
      email,
      name,
      phone,
      website,
      company,
      address,
      firstname,
      surname,
    };
  }
}
