import { TestBed } from '@angular/core/testing';
import { SortingService } from './sorting.service';
import { ISortDirection } from '../models/sort-direction';

describe('SortingService', () => {
  let service: SortingService;
  const mockData = [
    { id: 3, name: 'Nahuel' },
    { id: 1, name: 'Alejandro' },
    { id: 2, name: 'Carlos' },
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SortingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should sort data in ascending order', () => {
    const expectedData = [
      { id: 1, name: 'Alejandro' },
      { id: 2, name: 'Carlos' },
      { id: 3, name: 'Nahuel' },
    ];
    const sortedData = service.sort(mockData, 'name', ISortDirection.Asc);
    expect(sortedData).toEqual(expectedData);
  });

  it('should sort data in descending order', () => {
    const expectedData = [
      { id: 3, name: 'Nahuel' },
      { id: 2, name: 'Carlos' },
      { id: 1, name: 'Alejandro' },
    ];
    const sortedData = service.sort(mockData, 'name', ISortDirection.Desc);
    expect(sortedData).toEqual(expectedData);
  });

  it('should return the same data when sorting an empty array', () => {
    const data: any[] = [];
    const sortedData = service.sort(data, 'name', ISortDirection.Asc);
    expect(sortedData).toEqual([]);
  });

  it('should handle sorting with null or undefined values', () => {
    const data = [
      { id: 3, name: 'Nahuel' },
      { id: 1, name: null },
      { id: 2, name: undefined },
    ];
    const expectedData = [
      { id: 3, name: 'Nahuel' },
      { id: 1, name: null },
      { id: 2, name: undefined },
    ];

    const sortedData = service.sort(data, 'name', ISortDirection.Asc);
    expect(sortedData).toEqual(expectedData);
  });
});
