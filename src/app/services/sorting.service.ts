import { Injectable } from '@angular/core';
import { ISortDirection } from '../models/sort-direction';

@Injectable({
  providedIn: 'root',
})
export class SortingService {
  sort<T>(data: T[], sortColumn: keyof T, sortDirection: ISortDirection): T[] {
    return data.sort((a, b) => {
      const aValue = a[sortColumn];
      const bValue = b[sortColumn];

      if (aValue < bValue) {
        return sortDirection === ISortDirection.Asc ? -1 : 1;
      } else if (aValue > bValue) {
        return sortDirection === ISortDirection.Asc ? 1 : -1;
      } else {
        return 0;
      }
    });
  }
}
