export enum ISortDirection {
  Asc = 'asc',
  Desc = 'desc',
}
