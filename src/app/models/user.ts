import { IAddress } from './address';
import { ICompany } from './company';
import { ISortDirection } from './sort-direction';

export interface IUser {
  id: number;
  email: string;
  name: string;
  username: string;
  phone: string;
  website: string;
  company: ICompany;
  address: IAddress;
}

export interface IUserExtended extends IUser {
  firstname: string;
  surname: string;
}

export enum IUserColumns {
  firstname = 'firstname',
  surname = 'surname',
  username = 'username',
  email = 'email',
}

export interface IUserSort {
  sortColumn: IUserColumns;
  sortDirection: ISortDirection;
}
