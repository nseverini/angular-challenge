import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  inject,
} from '@angular/core';
import { ApiService } from './services/api.service';
import { BehaviorSubject, Observable, combineLatest, map } from 'rxjs';
import { IUserColumns, IUserExtended, IUserSort } from './models/user';
import { SortingService } from './services/sorting.service';
import { ISortDirection } from './models/sort-direction';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  apiService = inject(ApiService);
  sortingService = inject(SortingService);

  users$!: Observable<IUserExtended[]>;
  filter$ = new BehaviorSubject<string>('');
  sort$ = new BehaviorSubject<IUserSort>({
    sortColumn: IUserColumns.firstname,
    sortDirection: ISortDirection.Asc,
  });

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(): void {
    this.users$ = combineLatest([
      this.apiService.getUsers(),
      this.filter$,
      this.sort$,
    ]).pipe(
      map(([users, filter, sorting]) => {
        const filteredUsers = this.filterUsers(users, filter);
        return this.sortingService.sort(
          filteredUsers,
          sorting.sortColumn,
          sorting.sortDirection
        );
      })
    );
  }

  filterUsers(users: IUserExtended[], filter: string): IUserExtended[] {
    return users.filter((user) =>
      user.name.toLowerCase().includes(filter.toLowerCase())
    );
  }

  applySearch(searchTerm: string): void {
    this.filter$.next(searchTerm);
  }

  applySorting(sorting: IUserSort): void {
    this.sort$.next(sorting);
  }
}
