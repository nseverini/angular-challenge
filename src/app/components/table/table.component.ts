import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { ISortDirection } from 'src/app/models/sort-direction';
import {
  IUser,
  IUserExtended,
  IUserColumns,
  IUserSort,
} from 'src/app/models/user';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableComponent {
  @Input() data: IUserExtended[] | null = [];
  @Input() columns: string[] = [];
  @Output() sort = new EventEmitter<IUserSort>();
  userSort: IUserSort = {
    sortColumn: IUserColumns.firstname,
    sortDirection: ISortDirection.Asc,
  };
  userColumnType = IUserColumns;

  userTrackBy(index: number, user: IUser): number {
    return user.id;
  }

  sortTable(sortColumn: IUserColumns): void {
    if (this.userSort.sortColumn === sortColumn) {
      this.userSort.sortDirection =
        this.userSort.sortDirection === ISortDirection.Asc
          ? ISortDirection.Desc
          : ISortDirection.Asc;
    } else {
      this.userSort.sortColumn = sortColumn;
      this.userSort.sortDirection = ISortDirection.Asc;
    }

    this.sort.emit(this.userSort);
  }
}
