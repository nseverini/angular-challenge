import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TableComponent } from './table.component';
import { ISortDirection } from 'src/app/models/sort-direction';
import { IUserColumns, IUserSort } from 'src/app/models/user';
import { SortDirectionPipe } from 'src/app/pipes/sort-direction.pipe';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TableComponent, SortDirectionPipe],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit sort event when sortTable is called', () => {
    const sortColumn: IUserColumns = IUserColumns.surname;
    const sortEmitSpy = spyOn(component.sort, 'emit');

    component.sortTable(sortColumn);

    const expectedSort: IUserSort = {
      sortColumn,
      sortDirection: ISortDirection.Asc,
    };
    expect(sortEmitSpy).toHaveBeenCalledWith(expectedSort);
  });

  it('should toggle sortDirection when sortTable is called with same sortColumn', () => {
    const sortColumn: IUserColumns = IUserColumns.firstname;
    component.userSort = {
      sortColumn,
      sortDirection: ISortDirection.Asc,
    };

    component.sortTable(sortColumn);
    expect(component.userSort.sortDirection).toBe(ISortDirection.Desc);

    component.sortTable(sortColumn);
    expect(component.userSort.sortDirection).toBe(ISortDirection.Asc);
  });

  it('should reset sortDirection to Asc when sortTable is called with different sortColumn', () => {
    const sortColumn: IUserColumns = IUserColumns.username;
    component.userSort = {
      sortColumn: IUserColumns.firstname,
      sortDirection: ISortDirection.Desc,
    };

    component.sortTable(sortColumn);
    expect(component.userSort.sortDirection).toBe(ISortDirection.Asc);
  });
});
