import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Subject, debounceTime, takeUntil } from 'rxjs';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchBoxComponent implements OnInit, OnDestroy {
  @Output() search = new EventEmitter<string>();
  private searchText$ = new Subject<string>();
  private onDestroy$ = new Subject<void>();

  ngOnInit(): void {
    this.searchText$
      .pipe(debounceTime(300), takeUntil(this.onDestroy$))
      .subscribe((searchText) => {
        this.search.emit(searchText);
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  onSearch(event: Event) {
    const searchTerm = (event.target as HTMLInputElement).value;
    this.searchText$.next(searchTerm);
  }
}
